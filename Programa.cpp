#include <iostream>
#include "Pila.h"

using namespace std;

void rellenar_planilla(Pila *planilla_x, Pila *planilla_y, int m, int n){

    int contenedor_fila;
    int contenedor_columna;
    int salir = 0;
    string planilla[n][m];
    string colocar;

    for (int i=0; i<n; i++){
        for (int j=0; j<m; j++){

            cout << "¿CONTENDOR? (S/N): ";
            cin >> colocar;

            if (colocar == "S" || colocar == "s"){
                cout << "SI se ha colocado un contenedor." << endl;
                planilla_x[j].set_contenedor(" x ");
                planilla_y[i].set_contenedor(" x ");
            }
            else if (colocar == "N" || colocar == "n"){
                cout << "NO se ha colocado ningún contenedor." << endl;
                planilla_x[j].set_contenedor("  ");
                planilla_y[i].set_contenedor("  ");
            }
            else{
                cout << "Error.";
            }
        }
        cout << "\n";
    }
}

void dibujar_planilla(Pila *planilla_x, Pila *planilla_y, int m,  int n){

    for (int i=0; i<n; i++){

        for (int j=0; j<m; j++){

            cout << "| " << planilla_x[j].get_contenedor() << "|";
            //cout << "| " << planilla_y[i].get_contenedor() << "|" << endl;
            //cout << "|  |";

        }

        //cout << "\n";

    }

}

int main(){

    string n;
    string m;

    cout << "¿ANCHO DE LA PLANILLA?: ";
    getline(cin, m);
    cout << "¿ALTO DE LA PLANILLA?: ";
    getline(cin, n);

    Pila planilla_x[stoi(m)];
    Pila planilla_y[stoi(n)];

    rellenar_planilla(planilla_x, planilla_y, stoi(m), stoi(n));
    dibujar_planilla(planilla_x, planilla_y, stoi(m), stoi(n));

    return 0;
}
