#include <iostream>

using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila{

    private:
        int x;
        string contenedor;

    public:
        Pila();
        void set_contenedor(string contenedor);
        string get_contenedor();


};
#endif
